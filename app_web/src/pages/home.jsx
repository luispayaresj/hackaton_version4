import { Banner } from '../components/navbar/banner';
import { ListadoMedicos } from '../components/medicos/listado_medicos';

export function Home() {


    return(
        
            
            <div>
                <div className='py-4 px-4'>
                    <h1 className=' display-1 py-5 fw-bolder text-center bg-dark text-principal bg-opacity-75 rounded-pill bg-gradient'>
                    APP Hospital </h1>


                </div>
                <Banner />
                
            </div>
        
    )

    // let [counter, setCounter] = useState(0)


    // return (
    //     <div>
    //         <h1>Home Page </h1>
    //         <div>

    //             <button
    //                 type="button"
    //                 className="btn btn-success"
    //                 onClick={() => {
    //                     setCounter(counter + 1)
    //                 }}
    //             >
    //                 Count +1
    //             </button>

    //             <button
    //                 type="button"
    //                 className="btn btn-danger"
    //                 onClick={() => {
    //                     setCounter(counter - 1)
    //                 }}
    //             >
    //                 Count -1
    //             </button>

    //             <div>This is the count so far : {counter}</div>

    //         </div>
    //     </div>
    // )
}