import axios from 'axios';
import bannerImg from '../assets/img/bannerImg.jpg'
import { useNavigate } from 'react-router-dom';
export default function Login() {
    const navigate = useNavigate();
    function userLogin(e) {
        console.log('Login');
        e.preventDefault();
        const form = new FormData(e.target);
        const values = Object.fromEntries(form.entries());

        axios.post('http://127.0.0.1:8000/api/token/', values).then((response) => {
            console.log(response.data);
            localStorage.setItem('token', response.data.access);
            navigate('/');
            window.location.reload();
        });
    }

    return (
        <div className="container">
            <div className='align-items-center mt-5 pt-5'>
                <div className='row'>
                    <div className='col-6'
                        style={{
                            backgroundSize: "cover",
                            backgroundImage: "url(https://picsum.photos/800/301)",
                        }}
                    ></div>

                    <div className='col-6'>
                        <h4>LOGIN</h4>
                        <h5>Ingresa tus credenciales para accer a los CRUD's</h5>
                        <form
                            onSubmit={(event) => {
                                userLogin(event);
                            }}
                        >
                            <div className='mb-4'>
                                <label className='form-label'>User</label>
                                <input
                                    type='text'
                                    className='form-control'
                                    id='username'
                                    name='username'
                                    aria-describedby='emailHelp'
                                    placeholder='Ingrese nombre de usuario...'
                                />
                            </div>
                            <div className='mb-3'>
                                <label className='form-label'>Password</label>
                                <input
                                    type='password'
                                    className='form-control'
                                    id='password'
                                    name='password'
                                    placeholder='Ingrese contraseña...'
                                />
                            </div>
                            <div className='text-center mt-4'>
                                <button type='submit' className='btn btn-success'>
                                    Login
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}
