import { useEffect, useState } from 'react';

import { Link } from 'react-router-dom';

export function NavBarComponent(){

    let token = localStorage.getItem('token');
    let [codigoSeguridad, setCodigoSeguridad] = useState(token);
    return (
        

        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-primary">
                <div className="container-fluid">

                    <Link className="navbar-brand" to="/">Home</Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    
                    {codigoSeguridad ? (
                        <div>
                            
                            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                    
                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            Medicos
                                        </a>
                                        <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><Link className="dropdown-item" to="/listadoMedicos">Listar</Link></li>
                                            <li><Link className="dropdown-item" to="/formularioMedicos">Registrar</Link></li>
                                        </ul>
                                    </li>

                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            Pacientes
                                        </a>
                                        <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><Link className="dropdown-item" to="/listadoPacientes">Listar</Link></li>
                                            <li><Link className="dropdown-item" to="/formularioPacientes">Registrar</Link></li>
                                        </ul>
                                    </li>

                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            Citas
                                        </a>
                                        <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><Link className="dropdown-item" to="/listadoCitas">Listar</Link></li>
                                            <li><Link className="dropdown-item" to="/formularioCitas">Registrar</Link></li>
                                        </ul>
                                    </li>

                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            Horarios
                                        </a>
                                        <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <li><Link className="dropdown-item" to="/listadoHorarios">Listar</Link></li>
                                            <li><Link className="dropdown-item" to="/formularioHorarios">Registrar</Link></li>
                                        </ul>
                                    </li>
                                </ul>
                                
                                <div className='nav-item'>
                                    <Link
                                        onClick={() => {
                                        localStorage.removeItem('token');
                                        window.location.reload();
                                        }}
                                        className='nav-link text-white'
                                        to='/'
                                    >
                                        Logout
                                    </Link>
                                </div>
                            </div>
                        </div>
                    
                    ) : 
                    
                    (
                        <div>    
                            
                            <div className='nav-item'>
                                    <Link className='nav-link text-black' to='/login'>
                                        Login
                                    </Link>
                            </div>   
                        </div> 
                    )}
                    
                    
                    

                        
                    </div>
                
            </nav>
        </div>
    )
}