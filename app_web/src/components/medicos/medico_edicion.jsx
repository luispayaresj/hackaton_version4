import { Button, Form, FormGroup, Input, Label } from 'reactstrap'
import { useEffect, useState } from 'react';

import { Link } from 'react-router-dom';
import axios from 'axios';
import { useParams } from 'react-router-dom';

let token = localStorage.getItem('token');
    const config = {
        headers: { Authorization: `Bearer ${token}` }
    }
export default function ActualizarMedico() {
    //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU4OTU4NDc4LCJpYXQiOjE2NTQ2Mzg0NzgsImp0aSI6ImJjYzEwYjk4NDBmMzRlYmJiNzNiMzkzMjUyN2U5Y2MxIiwidXNlcl9pZCI6MX0.S4GZDS9AiciTu3b0F4KFUtRKvu4ixp5tL2ozKeoEvbw"
    let [codigoSeguridad, setCodigoSeguridad] = useState(token);
    let { MedicoId } = useParams();
    let [medico, setMedico] = useState([]);
    useEffect(() => {
        getMedico();
        },[]); 
    async function getMedico() {
        axios.get(`http://localhost:8000/api/hospital/medicos/${MedicoId}/`, config).then((response) => {
            setMedico(response.data);
            console.log(response);
    })};

    console.log(medico)
    return(
            <div className="container">

                {codigoSeguridad ? 
                
                <div>
                    <h1>Editando al doctor ${medico['nombres']}</h1>
                    <Form 
                                onSubmit={(event) => {
                                    Medico_Update(event, medico.id);
                                    window.location.href='http://localhost:3000/listadoMedicos';
                                }}
                    >
                        <FormGroup>
                            <Label for="usuarioDeRegistro">
                                Usuario de registro
                            </Label>
                            <Input
                                id="usuario_registro"
                                name="usuario_registro"
                                placeholder="Usuario de registro"
                                type="text"
                                defaultValue={medico.usuario_registro}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="Usuario de modificacion">
                                Usuario de modificacion
                            </Label>
                            <Input
                                id="usuario_modificacion"
                                name="usuario_modificacion"
                                placeholder="Usuario de modificacion"
                                type="text"
                                defaultValue={medico.usuario_modificacion}
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="Nombres">
                                Nombres
                            </Label>
                            <Input
                                id="nombres"
                                name="nombres"
                                placeholder="Nombre #1 Nombre #2"
                                type="text"
                                defaultValue={medico.nombres}
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="Apellidos">
                                Apellidos
                            </Label>
                            <Input
                                id="apellidos"
                                name="apellidos"
                                placeholder="Apellidos #1 Apellido #2"
                                type="text"
                                defaultValue={medico.apellidos}
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="DNI">
                                DNI
                            </Label>
                            <Input
                                id="dni"
                                name="dni"
                                placeholder="Numero de identificacion"
                                type="text"
                                defaultValue={medico.dni}
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="Fecha de nacimiento">
                                Fecha de nacimiento
                            </Label>
                            <Input
                                id="fecha_nacimiento"
                                name="fecha_nacimiento"
                                type="date"
                                defaultValue={medico.fecha_nacimiento}
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="Direccion">
                                Direccion
                            </Label>
                            <Input
                                id="direccion"
                                name="direccion"
                                placeholder="direccion"
                                type="text"
                                defaultValue={medico.direccion}
                            />
                        </FormGroup>


                        <FormGroup>
                            <Label for="Telefono">
                                Telefono
                            </Label>
                            <Input
                                id="telefono"
                                name="telefono"
                                placeholder="numero de celular o telefono"
                                type="text"
                                defaultValue={medico.telefono}
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="Sexo">
                                Genero
                            </Label>
                            <Input
                                id="sexo"
                                name="sexo"
                                placeholder="Ingrese genero"
                                type="text"
                                defaultValue={medico.sexo}
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="Correo">
                                Correo Electronico
                            </Label>
                            <Input
                                id="correo"
                                name="correo"
                                placeholder="e-mail"
                                type="email"
                                defaultValue={medico.correo}
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="Numero de colegiatura">
                                Numero de colegiatura
                            </Label>
                            <Input
                                id="numcolegiatura"
                                name="numcolegiatura"
                                placeholder="Numero de colegiatura"
                                type="text"
                                defaultValue={medico.numcolegiatura}
                            />
                        </FormGroup>

                        <Button >
                            Submit
                        </Button>

                        <Link className="btn btn-danger" to="/listadoMedicos">Cancel</Link>
                    </Form>
                </div> 
                
                
                : <h1>Necesita logearse para acceder a este formulario</h1> 
                
                }
                
                
        </div>

    ) 
}

function Medico_Update(e,id) {
    e.preventDefault();
    const form = new FormData(e.target);
    const valores = Object.fromEntries(form.entries());

    axios
        .put(`http://localhost:8000/api/hospital/medicos/${id}/`,valores, config)
        .then((response) => {
        alert(`Doctor ${response.data['nombres']} editado con exito`);
        });
    }
