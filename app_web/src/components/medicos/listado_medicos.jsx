import { MedicoRegistro } from './medico_registro';
import axios from 'axios'
import { useEffect } from 'react';
import { useState } from 'react';
export function ListadoMedicos() {

    let [medicos, setMedicos] = useState([])

    //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU4OTU4NDc4LCJpYXQiOjE2NTQ2Mzg0NzgsImp0aSI6ImJjYzEwYjk4NDBmMzRlYmJiNzNiMzkzMjUyN2U5Y2MxIiwidXNlcl9pZCI6MX0.S4GZDS9AiciTu3b0F4KFUtRKvu4ixp5tL2ozKeoEvbw"
    let token = localStorage.getItem('token');
    const config = {
        headers: { Authorization: `Bearer ${token}` },
    };

    async function getMedicos(){
        
            axios.get('http://localhost:8000/api/hospital/medicos/', config).then((response) => {
            setMedicos(response.data)
            console.log(response)
            
        })
        
    }
    //console.log("este es medicos")
    //console.log(medicos)
    useEffect(() => {
        getMedicos()
    }, [])



    return (


        <div className="container">
            <h2>Medicos</h2>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombres</th>
                        <th scope="col">Apellidos</th>
                        <th scope="col">DNI</th>
                    </tr>
                </thead>
                {
                    medicos ? medicos.map((medico)=>{
                        return(
                            <MedicoRegistro key={medico.id} {...medico}/>
                        )
                    }) : "cargando medicos..."
                }
            </table>

        </div>

    )
}
