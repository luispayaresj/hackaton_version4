import { Button, Form, FormGroup, Input, Label } from 'reactstrap'

import { Link } from 'react-router-dom';
import axios from 'axios'
import { useState } from 'react';

let token = localStorage.getItem('token');
const config = {
    headers: { Authorization: `Bearer ${token}` },
};
export function PacienteForm() {
    let [codigoSeguridad, setCodigoSeguridad] = useState(token);
    return (
        <div className="container">
            {codigoSeguridad ? 
            
            
            <div>
                <h1>Registre a un nuevo Paciente</h1>
                <Form 
                    onSubmit=
                    {
                        (event)=>{
                            SendInfo(event);
                            window.location.href='http://localhost:3000/listadoPacientes';
                        }
                        
                    }
                >
                    <FormGroup>
                        <Label for="usuarioDeRegistro">
                            Usuario de registro
                        </Label>
                        <Input
                            id="usuario_registro"
                            name="usuario_registro"
                            placeholder="Usuario de registro"
                            type="text"
                            
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="Usuario de modificacion">
                            Usuario de modificacion
                        </Label>
                        <Input
                            id="usuario_modificacion"
                            name="usuario_modificacion"
                            placeholder="Usuario de modificacion"
                            type="text"
                            
                        />
                    </FormGroup>

                    <FormGroup>
                        <Label for="Nombres">
                            Nombres
                        </Label>
                        <Input
                            id="nombres"
                            name="nombres"
                            placeholder="Nombre #1 Nombre #2"
                            type="text"
                            
                        />
                    </FormGroup>

                    <FormGroup>
                        <Label for="Apellidos">
                            Apellidos
                        </Label>
                        <Input
                            id="apellidos"
                            name="apellidos"
                            placeholder="Apellidos #1 Apellido #2"
                            type="text"
                            
                        />
                    </FormGroup>

                    <FormGroup>
                        <Label for="DNI">
                            DNI
                        </Label>
                        <Input
                            id="dni"
                            name="dni"
                            placeholder="Numero de identificacion"
                            type="text"
                            
                        />
                    </FormGroup>

                    <FormGroup>
                        <Label for="Fecha de nacimiento">
                            Fecha de nacimiento
                        </Label>
                        <Input
                            id="fecha_nacimiento"
                            name="fecha_nacimiento"
                            type="date"
                            
                        />
                    </FormGroup>

                    <FormGroup>
                        <Label for="Direccion">
                            Direccion
                        </Label>
                        <Input
                            id="direccion"
                            name="direccion"
                            placeholder="direccion"
                            type="text"
                            
                        />
                    </FormGroup>


                    <FormGroup>
                        <Label for="Telefono">
                            Telefono
                        </Label>
                        <Input
                            id="telefono"
                            name="telefono"
                            placeholder="numero de celular o telefono"
                            type="text"
                            
                        />
                    </FormGroup>

                    <FormGroup>
                        <Label for="Sexo">
                            Genero
                        </Label>
                        <Input
                            id="sexo"
                            name="sexo"
                            placeholder="Ingrese genero"
                            type="text"
                            
                        />
                    </FormGroup>


                    <FormGroup>
                        <Label for="EPS">
                            EPS
                        </Label>
                        <Input
                            id="eps"
                            name="eps"
                            placeholder="Ingrese la EPS del PACIENTE"
                            type="text"
                            
                        />
                    </FormGroup>

                    <Button >
                        Submit
                    </Button>

                    <Link className="btn btn-danger" to="/listadoPacientes">Cancel</Link>
                </Form>
            </div>
            
            
            
            :<h1>Necesita logearse para acceder a este formulario</h1>}
            
        </div>
    )
}

function SendInfo(event) {
    event.preventDefault()
    const form = new FormData(event.target)
    const valores = Object.fromEntries(form.entries())
    
    //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU4OTU4NDc4LCJpYXQiOjE2NTQ2Mzg0NzgsImp0aSI6ImJjYzEwYjk4NDBmMzRlYmJiNzNiMzkzMjUyN2U5Y2MxIiwidXNlcl9pZCI6MX0.S4GZDS9AiciTu3b0F4KFUtRKvu4ixp5tL2ozKeoEvbw"
    
    axios.post('http://localhost:8000/api/hospital/pacientes/',valores ,config).then((response) => {
            alert(`El paciente ${response.data['nombres']} ${response.data['apellidos']} fue registrado con exito`)
            
        }) 
}