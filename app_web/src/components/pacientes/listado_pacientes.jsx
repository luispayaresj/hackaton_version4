import { PacienteRegistro } from './paciente_registro';
import axios from 'axios'
import { useEffect } from 'react';
import { useState } from 'react';
export function ListadoPacientes() {

    let [pacientes, setPacientes] = useState([])

    //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU4OTU4NDc4LCJpYXQiOjE2NTQ2Mzg0NzgsImp0aSI6ImJjYzEwYjk4NDBmMzRlYmJiNzNiMzkzMjUyN2U5Y2MxIiwidXNlcl9pZCI6MX0.S4GZDS9AiciTu3b0F4KFUtRKvu4ixp5tL2ozKeoEvbw"
    let token = localStorage.getItem('token');
    const config = {
        headers: { Authorization: `Bearer ${token}` },
    };

    async function getPacientes(){
        
            axios.get('http://localhost:8000/api/hospital/pacientes/', config).then((response) => {
            setPacientes(response.data)
            console.log(response)
        })
        
    }
    //console.log("este es medicos")
    //console.log(medicos)
    useEffect(() => {
        getPacientes()
    }, [])



    return (


        <div className="container">
            <h2>Pacientes</h2>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombres</th>
                        <th scope="col">Apellidos</th>
                        <th scope="col">DNI</th>
                    </tr>
                </thead>
                {
                    pacientes ? pacientes.map((paciente)=>{
                        return(
                            <PacienteRegistro key={paciente.id} {...paciente}/>
                        )
                    }) : "cargando paciente..."
                }
            </table>

        </div>

    )
}