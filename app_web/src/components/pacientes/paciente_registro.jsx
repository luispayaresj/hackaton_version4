import { Link } from 'react-router-dom';
import axios from 'axios';
import { useState } from 'react';
export function PacienteRegistro({id, nombres, apellidos, dni}){

    {
        let [pacienteId, setPacienteId] = useState(id);

        function eliminarPaciente() {

            //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU4OTU4NDc4LCJpYXQiOjE2NTQ2Mzg0NzgsImp0aSI6ImJjYzEwYjk4NDBmMzRlYmJiNzNiMzkzMjUyN2U5Y2MxIiwidXNlcl9pZCI6MX0.S4GZDS9AiciTu3b0F4KFUtRKvu4ixp5tL2ozKeoEvbw"
            let token = localStorage.getItem('token');
            const config = {
                    headers: { Authorization: `Bearer ${token}` }
            }
            console.log(pacienteId);
            axios
            .delete(`http://localhost:8000/api/hospital/pacientes/${pacienteId}/`,config)
            .then((response) => {
                console.log(response);
                alert(`El paciente ${response.data['nombres']} ha sido Eliminado, Recargue la pagina para actualizar cambios`);
            });
        }

    return(

            <tbody>
                    
                        <tr>
                            <th scope="row">{id}</th>
                            <td>{nombres}</td>
                            <td>{apellidos}</td>
                            <td>{dni}</td>
                            <td> 
                                <div className="btn-group" role="group" aria-label="Basic mixed styles example">

                                    <Link to={`/actualizar_paciente/${id}`} className='btn btn-success  btn-sm' >Actualizar</Link>
                                    <button  type="submit" onClick={() => {
                                        eliminarPaciente();
                                        window.location.reload();
                                    }} className='btn btn-secondary  btn-sm' >Borrar</button>
                                </div>
                            </td>

                        </tr>
                    

            </tbody>
        )
    }
}   