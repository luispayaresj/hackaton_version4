import { HorarioRegistro } from './horario_registro';
import axios from 'axios'
import { useEffect } from 'react';
import { useState } from 'react';
export function ListadoHorarios() {

    let [horarios, setHorarios] = useState([])

    //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU4OTU4NDc4LCJpYXQiOjE2NTQ2Mzg0NzgsImp0aSI6ImJjYzEwYjk4NDBmMzRlYmJiNzNiMzkzMjUyN2U5Y2MxIiwidXNlcl9pZCI6MX0.S4GZDS9AiciTu3b0F4KFUtRKvu4ixp5tL2ozKeoEvbw"
    let token = localStorage.getItem('token');
    const config = {
        headers: { Authorization: `Bearer ${token}` },
    };

    async function getHorarios(){
        
            axios.get('http://localhost:8000/api/hospital/horarios/', config).then((response) => {
            setHorarios(response.data)
            console.log(response)
        })
        
    }
    //console.log("este es medicos")
    //console.log(medicos)
    useEffect(() => {
        getHorarios()
    }, [])



    return (


        <div className="container">
            <h2>Horarios</h2>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Medico</th>
                        <th scope="col">Fecha de Atencion</th>
                        <th scope="col">Inicio de Atencion</th>
                        <th scope="col">Fin de Atencion</th>
                    </tr>
                </thead>
                {
                    horarios ? horarios.map((horario)=>{
                        return(
                            <HorarioRegistro key={horario.id} {...horario}/>
                        )
                    }) : "cargando horario..."
                }
            </table>

        </div>

    )
}