import { Button, Form, FormGroup, Input, Label } from 'reactstrap'
import {useEffect, useState} from 'react'

import { Link } from 'react-router-dom';
import axios from 'axios'

let token = localStorage.getItem('token');
    const config = {
        headers: { Authorization: `Bearer ${token}` },
    };
export function HorarioForm() {

    let [codigoSeguridad, setCodigoSeguridad] = useState(token);

    let [medicos, setMedicos] = useState([])

    //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU4OTU4NDc4LCJpYXQiOjE2NTQ2Mzg0NzgsImp0aSI6ImJjYzEwYjk4NDBmMzRlYmJiNzNiMzkzMjUyN2U5Y2MxIiwidXNlcl9pZCI6MX0.S4GZDS9AiciTu3b0F4KFUtRKvu4ixp5tL2ozKeoEvbw"
    
    async function getMedicos(){
        
            axios.get('http://localhost:8000/api/hospital/medicos/', config).then((response) => {
            setMedicos(response.data)
            
            
        })
        
    }
    //console.log("este es medicos")
    //console.log(medicos)
    useEffect(() => {
        getMedicos()
        
    }, [])

    console.log(medicos)

    
    return (
        <div className="container">
            {codigoSeguridad ? 
            
            <div>
                <h1>Registre a un nuevo Horario</h1>
                <Form 
                    onSubmit=
                    {
                        (event)=>{
                            SendInfo(event);
                            
                            window.location.href='http://localhost:3000/listadoHorarios';
                        }
                        
                    }
                >
                    <FormGroup>
                        <Label for="Usuario de registro">
                            Usuario de registro
                        </Label>
                        <Input
                            id="usuario_registro"
                            name="usuario_registro"
                            placeholder="Usuario de registro"
                            type="text"
                            
                        />
                    </FormGroup>
                    
                    <FormGroup>
                        <Label for="Usuario de modificacion">
                            Usuario de modificacion
                        </Label>
                        <Input
                            id="usuario_modificacion"
                            name="usuario_modificacion"
                            placeholder="Usuario de modificacion"
                            type="text"
                            
                        />
                    </FormGroup>
                    
                    <FormGroup>
                        <Label for="medico">
                            Medico
                        </Label>
                        <Input
                            bsSize="lg"
                            className="mb-3"
                            type="select"
                            id="medico_id"
                            name="medico_id"
                        >
                            {medicos? medicos.map((medico)=>{

                                return (
                                    <option key= {medico.id} value={medico.id}>
                                        {medico.nombres} {medico.apellidos}
                                    </option>
                                )
                            }) : 
                                "cargando medicos..."
                                
                                
                            
                            }
                        </Input>
                    </FormGroup>
                    
                    

                    <FormGroup>
                        <Label for="fecha de atencion">
                            Fecha de atencion
                        </Label>
                        <Input
                            id="fecha_atencion"
                            name="fecha_atencion"
                            placeholder="Fecha de atencion"
                            type="date"
                            
                        />
                    </FormGroup>

                    <FormGroup>
                        <Label for="Inicio de atencion">
                            Inicio de atencion
                        </Label>
                        <Input
                            id="inicio_atencion"
                            name="inicio_atencion"
                            placeholder="Inicio de atencion"
                            type="time"
                            
                        />
                    </FormGroup>

                    <FormGroup>
                        <Label for="Fin de atencion">
                            Fin de atencion
                        </Label>
                        <Input
                            id="fin_atencion"
                            name="fin_atencion"
                            placeholder="Fin de atencion"
                            type="time"
                            
                        />
                    </FormGroup>

                    <Button >
                        Submit
                    </Button>

                    <Link className="btn btn-danger" to="/listadoHorarios">Cancel</Link>
                </Form>
            </div>
            
            :<h1>Tienes que logearte para acceder a este formulario</h1>}
            
        </div>
    )
}

function SendInfo(event) {
    event.preventDefault()
    const form = new FormData(event.target)
    const valores = Object.fromEntries(form.entries())
    console.log(valores)
    //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU4OTU4NDc4LCJpYXQiOjE2NTQ2Mzg0NzgsImp0aSI6ImJjYzEwYjk4NDBmMzRlYmJiNzNiMzkzMjUyN2U5Y2MxIiwidXNlcl9pZCI6MX0.S4GZDS9AiciTu3b0F4KFUtRKvu4ixp5tL2ozKeoEvbw"
    
    axios.post('http://localhost:8000/api/hospital/horarios/',valores ,config).then((response) => {
            alert(`El horario fue registrado con exito`)
            console.log(response)
            
        }).catch((error) => {console.log(error)}) 
}