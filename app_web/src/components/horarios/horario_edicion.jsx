import { Button, Form, FormGroup, Input, Label } from 'reactstrap'
import { useEffect, useState } from 'react';

import { Link } from 'react-router-dom';
import axios from 'axios';
import { useParams } from 'react-router-dom';

let token = localStorage.getItem('token');
    const config = {
        headers: { Authorization: `Bearer ${token}` }
    }
export default function ActualizarHorario() {
    //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU4OTU4NDc4LCJpYXQiOjE2NTQ2Mzg0NzgsImp0aSI6ImJjYzEwYjk4NDBmMzRlYmJiNzNiMzkzMjUyN2U5Y2MxIiwidXNlcl9pZCI6MX0.S4GZDS9AiciTu3b0F4KFUtRKvu4ixp5tL2ozKeoEvbw"
    let [codigoSeguridad, setCodigoSeguridad] = useState(token);
    let { horarioId } = useParams();
    let [horario, setHorario] = useState([]);
    useEffect(() => {
        getHorario();
        },[]); 
    async function getHorario() {
        axios.get(`http://localhost:8000/api/hospital/horarios/${horarioId}/`, config).then((response) => {
            setHorario(response.data);
            console.log(response);
    })};

    let [medicos, setMedicos] = useState([])

    

    async function getMedicos(){
        
            axios.get('http://localhost:8000/api/hospital/medicos/', config).then((response) => {
            setMedicos(response.data)
            
            
        })
        
    }
    //console.log("este es medicos")
    //console.log(medicos)
    useEffect(() => {
        getMedicos()
        
    }, [])

    
    return(
            <div className="container">
                {codigoSeguridad ? 
                
                <div>
                    <h1>Editando al horario</h1>
                        <Form 
                                    onSubmit={(event) => {
                                        Horario_Update(event, horario.id);
                                        window.location.href='http://localhost:3000/listadoHorarios';
                                    }}
                        >
                            <FormGroup>
                            <Label for="Usuario de registro">
                                Usuario de registro
                            </Label>
                            <Input
                                id="usuario_registro"
                                name="usuario_registro"
                                placeholder="Usuario de registro"
                                type="text"
                                defaultValue={horario.usuario_registro}
                                
                            />
                        </FormGroup>
                        
                        <FormGroup>
                            <Label for="Usuario de modificacion">
                                Usuario de modificacion
                            </Label>
                            <Input
                                id="usuario_modificacion"
                                name="usuario_modificacion"
                                placeholder="Usuario de modificacion"
                                type="text"
                                defaultValue={horario.usuario_modificacion}
                            />
                        </FormGroup>
                        
                        <FormGroup>
                            <Label for="medico">
                                Medico
                            </Label>
                            <Input
                                bsSize="lg"
                                className="mb-3"
                                type="select"
                                id="medico_id"
                                name="medico_id"
                                defaultValue={horario.medico_id}
                            >
                                {medicos? medicos.map((medico)=>{

                                    return (<option key={medico.id} value={medico.id}>
                                    {medico.nombres}
                                    </option>)
                                }) : 
                                    "cargando medicos..."
                                    
                                    
                                
                                }
                            </Input>
                        </FormGroup>
                        
                        

                        <FormGroup>
                            <Label for="fecha de atencion">
                                Fecha de atencion
                            </Label>
                            <Input
                                id="fecha_atencion"
                                name="fecha_atencion"
                                placeholder="Fecha de atencion"
                                type="date"
                                defaultValue={horario.fecha_atencion}
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="Inicio de atencion">
                                Inicio de atencion
                            </Label>
                            <Input
                                id="inicio_atencion"
                                name="inicio_atencion"
                                placeholder="Inicio de atencion"
                                type="time"
                                defaultValue={horario.inicio_atencion}
                                //{()=>
                                //     {const datetime = new Date(horario.inicio_atencion)
                                //         return  datetime.getFullYear()  + "-" + (datetime.getMonth()+1) + "-" + datetime.getDate()  + "T" +datetime.getHours() + ":" + datetime.getMinutes();
                                //     }}
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="Fin de atencion">
                                Fin de atencion
                            </Label>
                            <Input
                                id="fin_atencion"
                                name="fin_atencion"
                                placeholder="Fin de atencion"
                                type="time"
                                defaultValue={horario.fin_atencion}
                            />
                        </FormGroup>

                            <Button >
                                Submit
                            </Button>
                            
                            <Link className="btn btn-danger" to="/listadoHorarios">Cancel</Link>
                    </Form>
                </div> 
                
                
                
                : <h1>No puede acceder a este formularo, necesita logearse</h1>  }
                

                
        </div>

    ) 
}

function Horario_Update(e,id) {
    e.preventDefault();
    const form = new FormData(e.target);
    const valores = Object.fromEntries(form.entries());

    //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU4OTU4NDc4LCJpYXQiOjE2NTQ2Mzg0NzgsImp0aSI6ImJjYzEwYjk4NDBmMzRlYmJiNzNiMzkzMjUyN2U5Y2MxIiwidXNlcl9pZCI6MX0.S4GZDS9AiciTu3b0F4KFUtRKvu4ixp5tL2ozKeoEvbw"

    axios
        .put(`http://localhost:8000/api/hospital/horarios/${id}/`,valores, config)
        .then((response) => {
        alert(`Horario editado con exito`);
        
        });
    }
