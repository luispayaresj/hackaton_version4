import { Button, Form, FormGroup, Input, Label } from 'reactstrap'
import { useEffect, useState } from 'react';

import { Link } from 'react-router-dom';
import axios from 'axios';
import { useParams } from 'react-router-dom';

let token = localStorage.getItem('token');
const config = {
    headers: { Authorization: `Bearer ${token}` }
}
export default function ActualizarCita() {
    //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU4OTU4NDc4LCJpYXQiOjE2NTQ2Mzg0NzgsImp0aSI6ImJjYzEwYjk4NDBmMzRlYmJiNzNiMzkzMjUyN2U5Y2MxIiwidXNlcl9pZCI6MX0.S4GZDS9AiciTu3b0F4KFUtRKvu4ixp5tL2ozKeoEvbw"
    let [codigoSeguridad, setCodigoSeguridad] = useState(token);
    let { citaId } = useParams();
    let [cita, setCita] = useState([]);
    useEffect(() => {
        getCita();
    }, []);

    async function getCita() {
        axios.get(`http://localhost:8000/api/hospital/citas/${citaId}/`, config).then((response) => {
            setCita(response.data);
            console.log(response);
        })
    };

    let [medicos, setMedicos] = useState([])



    async function getMedicos() {

        axios.get('http://localhost:8000/api/hospital/medicos/', config).then((response) => {
            setMedicos(response.data)


        })

    }
    //console.log("este es medicos")
    //console.log(medicos)
    useEffect(() => {
        getMedicos()

    }, [])

    let [pacientes, setPacientes] = useState([]);

    useEffect(() => {
        getPacientes();
    }, []);

    async function getPacientes() {
        axios.get(`http://localhost:8000/api/hospital/pacientes/`, config).then((response) => {
            setPacientes(response.data);
            console.log(response);
        })
    };


    return (
        <div className="container">

            {codigoSeguridad ?

                <div>
                    <h1>Editando cita</h1>
                    <Form
                        onSubmit={(event) => {
                            Cita_Update(event, cita.id);
                            window.location.href = 'http://localhost:3000/listadoCitas';
                        }}
                    >
                        <FormGroup>
                            <Label for="Usuario de registro">
                                Usuario de registro
                            </Label>
                            <Input
                                id="usuario_registro"
                                name="usuario_registro"
                                placeholder="Usuario de registro"
                                type="text"
                                defaultValue={cita.usuario_registro}

                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="Usuario de modificacion">
                                Usuario de modificacion
                            </Label>
                            <Input
                                id="usuario_modificacion"
                                name="usuario_modificacion"
                                placeholder="Usuario de modificacion"
                                type="text"
                                defaultValue={cita.usuario_modificacion}
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="medico">
                                Medico
                            </Label>
                            <Input
                                bsSize="lg"
                                className="mb-3"
                                type="select"
                                id="medico_id"
                                name="medico_id"
                                defaultValue={cita.medico_id}
                            >
                                {medicos ? medicos.map((medico) => {

                                    return (<option key={medico.id} value={medico.id}>
                                        {medico.nombres}
                                    </option>)
                                }) :
                                    "cargando medicos..."



                                }
                            </Input>
                        </FormGroup>


                        <FormGroup>
                            <Label for="paciente">
                                Paciente
                            </Label>
                            <Input
                                bsSize="lg"
                                className="mb-3"
                                type="select"
                                id="paciente_id"
                                name="paciente_id"
                                defaultValue={cita.paciente_id}
                            >
                                {pacientes ? pacientes.map((paciente) => {

                                    return (<option key={paciente.id} value={paciente.id}>
                                        {paciente.nombres}
                                    </option>)
                                }) :
                                    "cargando pacientes..."



                                }
                            </Input>
                        </FormGroup>


                        <FormGroup>
                            <Label for="fecha de atencion">
                                Fecha de atencion
                            </Label>
                            <Input
                                id="fecha_atencion"
                                name="fecha_atencion"
                                placeholder="Fecha de atencion"
                                type="date"
                                defaultValue={cita.fecha_atencion}
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="Inicio de atencion">
                                Inicio de atencion
                            </Label>
                            <Input
                                id="inicio_atencion"
                                name="inicio_atencion"
                                placeholder="Inicio de atencion"
                                type="time"
                                defaultValue={cita.inicio_atencion}
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="Fin de atencion">
                                Fin de atencion
                            </Label>
                            <Input
                                id="fin_atencion"
                                name="fin_atencion"
                                placeholder="Fin de atencion"
                                type="time"
                                defaultValue={cita.fin_atencion}
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="Estado">
                                Estado
                            </Label>
                            <Input
                                id="estado"
                                name="estado"
                                placeholder="Estado de la cita"
                                type="text"
                                defaultValue={cita.estado}
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="Observaciones">
                                Observaciones
                            </Label>
                            <Input
                                id="observaciones"
                                name="observaciones"
                                placeholder="Observaciones"
                                type="textarea"
                                defaultValue={cita.observaciones}
                            />
                        </FormGroup>

                        <Button >
                            Submit
                        </Button>

                        <Link className="btn btn-danger" to="/listadoCitas">Cancel</Link>
                    </Form>
                </div>




                : <h1>Necesita logearse para acceder a este formulario</h1>

            }

        </div>

    )
}

function Cita_Update(e, id) {
    e.preventDefault();
    const form = new FormData(e.target);
    const valores = Object.fromEntries(form.entries());

    //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU4OTU4NDc4LCJpYXQiOjE2NTQ2Mzg0NzgsImp0aSI6ImJjYzEwYjk4NDBmMzRlYmJiNzNiMzkzMjUyN2U5Y2MxIiwidXNlcl9pZCI6MX0.S4GZDS9AiciTu3b0F4KFUtRKvu4ixp5tL2ozKeoEvbw"
    axios
        .put(`http://localhost:8000/api/hospital/citas/${id}/`, valores, config)
        .then((response) => {
            alert(`Cita editado con exito`);

        });
}
