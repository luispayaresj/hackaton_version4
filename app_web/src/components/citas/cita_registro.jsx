import { useEffect, useState } from 'react';

import { Link } from 'react-router-dom';
import axios from 'axios';

export function CitaRegistro({id, medico_id,paciente_id, fecha_atencion, inicio_atencion, fin_atencion, estado, observaciones}){

    {
        let [citaId, setCitasId] = useState(id);

        let token = localStorage.getItem('token');
            const config = {
                    headers: { Authorization: `Bearer ${token}` }
            }
            
        function eliminarCita() {

            //let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU4OTU4NDc4LCJpYXQiOjE2NTQ2Mzg0NzgsImp0aSI6ImJjYzEwYjk4NDBmMzRlYmJiNzNiMzkzMjUyN2U5Y2MxIiwidXNlcl9pZCI6MX0.S4GZDS9AiciTu3b0F4KFUtRKvu4ixp5tL2ozKeoEvbw"
            
            console.log(citaId);
            axios
            .delete(`http://localhost:8000/api/hospital/citas/${citaId}/`,config)
            .then((response) => {
                console.log(response);
                alert(`La cita  ha sido eliminada, Recargue la pagina para actualizar cambios`);
            });
            
                }
        
        let [medico, setMedico] = useState({});
            useEffect(() => {
                getMedico();
                },[]); 
            async function getMedico() {
                axios.get(`http://localhost:8000/api/hospital/medicos/${medico_id}/`, config).then((response) => {
                    setMedico(response.data);
                    console.log(response);
            })};

        
        let [paciente, setPaciente] = useState([]);
            useEffect(() => {
                getPaciente();
                },[]); 
            async function getPaciente() {
                axios.get(`http://localhost:8000/api/hospital/pacientes/${paciente_id}/`, config).then((response) => {
                    setPaciente(response.data);
                    console.log(response);
            })};
        
    return(

            <tbody>
                    
                        <tr>
                            <th scope="row">{id}</th>
                            <td>{medico.nombres }</td>
                            <td>{paciente.nombres}</td>
                            <td>{fecha_atencion}</td>
                            <td>{inicio_atencion}</td>
                            <td>{fin_atencion}</td>
                            <td>{estado}</td>
                            <td>{observaciones}</td>
                            <td> 
                                <div className="btn-group" role="group" aria-label="Basic mixed styles example">

                                    <Link to={`/actualizar_cita/${id}`} className='btn btn-success  btn-sm' >Actualizar</Link>
                                    <button  type="submit" onClick={() => {
                                        eliminarCita();
                                        window.location.reload();
                                    }} className='btn btn-secondary  btn-sm' >Borrar</button>
                                </div>
                            </td>

                        </tr>
                    

            </tbody>
        )
    }
}   