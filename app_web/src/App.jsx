import './App.css'
import { } from './logo.svg'

import { Route, Routes } from 'react-router-dom';

import ActualizarCita from './components/citas/citas_edicion';
import ActualizarHorario from './components/horarios/horario_edicion';
import ActualizarMedico from './components/medicos/medico_edicion';
import ActualizarPaciente from './components/pacientes/paciente_edicion';
import { CitaForm } from './components/citas/cita_formulario';
import { Home } from './pages/home'
import { HorarioForm } from './components/horarios/horario_formulario';
import { ListadoCitas } from './components/citas/listado_citas';
import { ListadoHorarios } from './components/horarios/listado_horarios';
import { ListadoMedicos } from './components/medicos/listado_medicos';
import { ListadoPacientes } from './components/pacientes/listado_pacientes';
import Login from './pages/login'
import { MedicoForm } from './components/medicos/medico_formulario'
import { NavBarComponent } from './components/navbar/navbar'
import { PacienteForm } from './components/pacientes/paciente_formulario';
import { useState } from 'react'

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">

      <NavBarComponent />
      <Routes>

        
        <Route path='/formularioMedicos' element={<MedicoForm />} />
        <Route path='/listadoMedicos' element={<ListadoMedicos />} />
        <Route path='/' element={<Home />} />
        <Route path='/actualizar_medico/:MedicoId' element={<ActualizarMedico />} />
        <Route path='/login' element={<Login />} />

        <Route path='/formularioPacientes' element={<PacienteForm />} />
        <Route path='/listadoPacientes' element={<ListadoPacientes />} />
        <Route path='/actualizar_paciente/:pacienteId' element={<ActualizarPaciente />} />

        <Route path='/formularioHorarios' element={<HorarioForm />} />
        <Route path='/listadoHorarios' element={<ListadoHorarios />} />
        <Route path='/actualizar_horario/:horarioId' element={<ActualizarHorario />} />

        <Route path='/formularioCitas' element={<CitaForm />} />
        <Route path='/listadoCitas' element={<ListadoCitas />} />
        <Route path='/actualizar_cita/:citaId' element={<ActualizarCita />} />
        
      </Routes>

    </div>
  )
}

export default App
