from rest_framework import routers

from in_hospital import views

router = routers.DefaultRouter()
router.register(r'medicos', views.MedicoViewSet)
router.register(r'pacientes', views.PacienteViewSet)
router.register(r'medicos_especialidades', views.MedicoEspecialidadViewSet)
router.register(r'citas', views.CitaViewSet)
router.register(r'especialidades', views.EspecialidadViewSet)
router.register(r'horarios', views.HorarioViewSet)