from django.db import models


# Create your models here.

class CommonInfo(models.Model):
    fecha_registro = models.DateField(auto_now_add=True)
    fehca_modificacion = models.DateField(auto_now=True)
    usuario_registro = models.CharField(max_length=200)
    usuario_modificacion = models.CharField(max_length=200)
    activo = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Personas(models.Model):
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    dni = models.CharField(max_length=50)
    fecha_nacimiento = models.DateField()
    direccion = models.CharField(max_length=100)
    telefono = models.CharField(max_length=20)
    sexo = models.CharField(max_length=20)

    class Meta:
        abstract = True


class Medico(Personas, CommonInfo):
    correo = models.EmailField()
    numcolegiatura = models.CharField(max_length=50)
    def __str__(self):
        return f'{self.nombres} {self.apellidos}'


class Paciente(Personas, CommonInfo):
    eps = models.CharField(max_length=200, null=True, blank=True)
    def __str__(self):
        return f'{self.nombres} {self.apellidos}'


class Horario(CommonInfo):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE)
    fecha_atencion = models.DateField()
    inicio_atencion = models.TimeField()
    fin_atencion = models.TimeField()


class Especialidad(CommonInfo):
    nombre = models.CharField(max_length=200)
    descripcion = models.TextField()


class MedicoEspecialidad(CommonInfo):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE)
    especialidad = models.ForeignKey(Especialidad, on_delete=models.CASCADE)



class Cita(CommonInfo):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE)
    paciente_id = models.ForeignKey(Paciente, on_delete=models.CASCADE)
    fecha_atencion = models.DateField()
    inicio_atencion = models.TimeField()
    fin_atencion = models.TimeField()
    estado = models.CharField(max_length=100)
    observaciones = models.TextField()
