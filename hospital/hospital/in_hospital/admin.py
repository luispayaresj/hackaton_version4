from django.contrib import admin

# Register your models here.
from in_hospital.models import Medico, Paciente, Horario, Especialidad, MedicoEspecialidad, Cita


# Register your models here.

@admin.register(Medico)
class MedicosAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombres', 'apellidos', 'dni', 'fecha_nacimiento')


@admin.register(Paciente)
class PacientesAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombres', 'apellidos', 'dni', 'fecha_nacimiento', 'eps')


@admin.register(Horario)
class HorariosAdmin(admin.ModelAdmin):
    list_display = ('id', 'medico_id', 'fecha_atencion', 'inicio_atencion', 'fin_atencion')


@admin.register(Cita)
class CitasAdmin(admin.ModelAdmin):
    list_display = ('id', 'medico_id','paciente_id', 'fecha_atencion', 'inicio_atencion', 'fin_atencion','estado','observaciones')

@admin.register(Especialidad)
class EspecialidadesAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'descripcion')

@admin.register(MedicoEspecialidad)
class MedicosEspecialidadesAdmin(admin.ModelAdmin):
    list_display = ('id', 'medico_id', 'especialidad')